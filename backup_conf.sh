# заносим в переменную DATE текущую дату 
DATE=`/bin/date '+%d.%m.%Y'`

# переходим в каталог, в котором находятся рабочие скрипты
mkdir /var/pre-backup
mkdir /var/backup

scp ansi-root@192.168.0.100:/etc/corosync/corosync.conf /var/pre-backup
scp ansi-root@192.168.0.100:/etc/nginx/nginx.conf /var/pre-backup

cd /var/pre-backup
# архивируем все каталоги, кроме тех, в которых находится файл .noarchive 
/bin/tar -zcf /var/backup/backup_node1_$DATE.tar.gz --exclude-tag=.noarchive ./

scp ansi-root@192.168.0.102:/etc/corosync/corosync.conf /var/pre-backup
cd /var/pre-backup
/bin/tar -zcf /var/backup/backup_node2_$DATE.tar.gz --exclude-tag=.noarchive ./
# удаляем архивы, которым уже больше семи дней 
/usr/bin/find /var/backup/ -type f -atime +7 -exec rm -f \{\} \; >/dev/null 2>&1

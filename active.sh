#!/bin/bash

service_list=("pacemaker" "corosync" "pcsd" "nginx" "sshd")

for (( i = 0; i <= 5; i++))
do
COMMAND=`/bin/systemctl status ${service_list[$i]} | grep Active | sed -r 's/.*\((.*)\).*/\1/'`
if [[ "$COMMAND" == *'dead'* ]]; then
    echo "${service_list[$i]} : Not running"
elif [[ "$COMMAND" == *'running'* ]]; then
    echo "${service_list[$i]} : Running"
fi;
done;
